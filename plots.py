import sox
import tempfile
import os
from matplotlib import pyplot as plt
import numpy as np
from scipy.io.wavfile import read
from scipy.fftpack import fft
import scipy.optimize as opt
import scipy.integrate as integrate

from tools import time_average

def t_plot(f):
    wav = read(f)
    data = np.array(wav[1],dtype=float)
    
    # Number of samplepoints
    N = len(data[:,0])
    # sample spacing
    T = 1.0 / wav[0]
    x = np.linspace(0.0, N*T, N)
    y = np.abs(data[:,1])

    plt.clf()
#     plt.plot(x, data[:,0])
#     plt.plot(x, data[:,1])
#     plt.plot(x, y)
    plt.plot(x, time_average(y, 0.05, wav[0]), 'r')
    
    plt.show()
    
def f_plot(f):
    wav = read(f)
    data = np.array(wav[1],dtype=float)
    
    # Number of samplepoints
    N = len(data[:,0])
    # sample spacing
    T = 1.0 / wav[0]
    x = np.linspace(0.0, N*T, N)

    yf0 = fft(data[:,0])
    yf1 = fft(data[:,1])
    xf = np.linspace(0.0, 1.0/(2.0*T), N/2)

    fig, ax = plt.subplots()
    ax.loglog(xf, 2.0/N * np.abs(yf0[:N//2]))
    ax.loglog(xf, 2.0/N * np.abs(yf1[:N//2]))
    ax.set_xlim([50, 2e4])
#     ax.set_ylim([1, 1.5e3])
    ax.grid()
    plt.show()