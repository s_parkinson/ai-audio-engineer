to get this to work!

1. install sox: 

   on mac this is

    brew install vorbis-tools
    brew install sox --with-lame --with-flac --with-libvorbis

2. create a virtual env and install the dependencies:

   again, on mac

    cd $THIS_DIR
    virtualenv venv --python=python3 && activate && pip install -r requirements.txt

3. run the jupyter notebook

    jupyter notebook
