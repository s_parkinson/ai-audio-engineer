from scipy.io.wavfile import read
import sox
import numpy as np

def compare_levels(f1, f2):
    wav1 = read(f1)
    data1 = np.array(wav1[1],dtype=float)
    y1 = np.log(data1[:,1].max()-data1[:,1].min())

    wav2 = read(f2)
    data2 = np.array(wav2[1],dtype=float)
    y2 = np.log(data2[:,1].max()-data2[:,1].min())

    # print(y1, y2)
    gain = y2-y1
    return gain

def leveller(f1, f2):
    gain = compare_levels(f1, f2)
    tf = sox.Transformer()
    # print('applying gain of', gain)
    tf.gain(gain_db=gain, normalize=True)
    return tf