import sox
import tempfile
import os
from matplotlib import pyplot as plt
import numpy as np
from scipy.io.wavfile import read
from scipy.fftpack import fft
import scipy.optimize as opt
import scipy.integrate as integrate

def time_average(signal, window_length, sample_rate):
    def window(size):
        return np.ones(size)/float(size)
    
    return np.convolve(signal, window(int(window_length*sample_rate)), 'same')