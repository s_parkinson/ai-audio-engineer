import sox
import tempfile
import os
from matplotlib import pyplot as plt
import numpy as np
from scipy.io.wavfile import read
from scipy.fftpack import fft
import scipy.optimize as opt
import scipy.integrate as integrate

from tools import time_average

def compress(x, scaling):
    attack, attack_decay, knee, tf00, tf01, tf0_10, tf0_11, tf1_20, tf1_21 = scaling*np.array(x)
    tf_points = [
            (tf00, tf01), 
            (min(tf00+tf0_10,-1), 
             min(tf01+tf0_11,-1)), 
            (min(tf00+tf0_10+tf1_20,0), 
             min(tf01+tf0_11+tf1_21,0))
        ]
    
    tf = sox.Transformer()
    tf.compand(
        attack_time=attack, 
        decay_time=attack+attack_decay, 
        soft_knee_db=knee, 
        tf_points=tf_points
    )
    return tf

def compression_lse(f1, f2):
    wav1 = read(f1)
    data1 = np.array(wav1[1],dtype=float)
    y1 = np.abs(data1[:,1])
    
    wav2 = read(f2)
    data2 = np.array(wav2[1],dtype=float)
    y2 = np.abs(data2[:,1])
    
    time_averaged_1 = time_average(y1, 0.05, wav1[0])
    time_averaged_2 = time_average(y2, 0.05, wav2[0])

    l = min(len(time_averaged_1), len(time_averaged_2))

    time_averaged_1 = time_averaged_1[:l]
    time_averaged_2 = time_averaged_2[:l]
    
    return ((time_averaged_1-time_averaged_2)**2).sum()

def cf(x, *args):    
    original, target, scaling = args
    temp = tempfile.mktemp()+'.wav'
    tf = compress(x, scaling)
    try:
        os.remove(temp)
    except:
        None
        
    success = tf.build(original, temp)
    val = compression_lse(target, temp)
    return val
    
def optimise_compression(source_file, target_file, scaling=None, callback=None):
    if scaling is None:
        scaling = np.array(
            [0.2, 0.6, 3, -80, -80, 30, 30, 50, 50]
        )

    bounds = np.array(
        [[0.01, 0.4], [0.0001, 0.8], [0, 12], [-150, -50], [-150, -10], [1, 150], [1, 150], [1, 150], [1, 150]]
    )
    bounds = np.array((bounds.T[0], bounds.T[1])/scaling).T
    bounds.sort(axis=1)

    # return opt.minimize(
    #     cf, 
    #     np.ones(scaling.shape), 
    #     (source_file, target_file, scaling), 
    #     method="L-BFGS-B", 
    #     bounds=bounds,
    #     callback=callback,
    #     options={
    #         'eps': 1e-1, 'ftol': 1e-9, 'gtol': 1e-07
    #     }
    # )

    class RandomDisplacementBounds(object):
        """random displacement with bounds"""
        def __init__(self, xmin, xmax, stepsize=0.5):
            self.xmin = xmin
            self.xmax = xmax
            self.stepsize = stepsize

        def __call__(self, x):
            """take a random step but ensure the new position is within the bounds"""
            while True:
                # this could be done in a much more clever way, but it will work for example purposes
                xnew = x + np.random.uniform(-self.stepsize, self.stepsize, np.shape(x))
                if np.all(xnew < self.xmax) and np.all(xnew > self.xmin):
                    break
            return xnew

    # define the new step taking routine and pass it to basinhopping
    take_step = RandomDisplacementBounds(bounds.T[0], bounds.T[1])
    minimizer_kwargs = {
        "args": (source_file, target_file, scaling), 
        "method": "L-BFGS-B", "bounds": bounds,
        # "options": {
        #     'eps': 1e-2, 'ftol': 1e-2
        # }
    }

    # print('starting optimisation...')
    return opt.basinhopping(cf, np.ones(scaling.shape), minimizer_kwargs=minimizer_kwargs,
                            niter=20, take_step=take_step, callback=callback)