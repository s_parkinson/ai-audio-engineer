import numpy as np
import numpy as np
import soundfile as sf
import matplotlib.pyplot as plt
from scipy.linalg import circulant
from scipy.io.wavfile import read as read_wav
from scipy.io.wavfile import write as write_wav
from scipy.signal import freqz
from scipy.interpolate import interp1d
from scipy.signal import stft, istft
from scipy.io.wavfile import write as write_wav
from scipy.io.wavfile import read


def apply_equalizer(v_raw, transfer_function):
   f, t, Zxx = stft(v_raw, 44100)
   Zxx_mod = np.zeros(Zxx.shape).astype('cfloat')
   mult = transfer_function(f)
   for i in range(Zxx.shape[1]):
       Zxx_mod[:,i] = np.multiply(mult, np.real(Zxx[:,i])) + np.multiply(1j, np.multiply(mult, np.imag(Zxx[:,i])))
   t, v_mod = istft(Zxx_mod)
   return t, v_mod
       

def estimate_FIR_response(v_raw, v_target, P):
   # Create system matrix
   A = np.reshape(v_raw, (len(v_raw), 1))
   for i in range(P):
       v_raw_shifted = np.roll(v_raw, i)
       A = np.hstack((A, np.reshape(v_raw_shifted, (len(v_raw), 1))))
   fir_response = np.linalg.pinv(A).dot(v_target)
   return fir_response


def estimate_frequency_response(v_raw, v_target, P):
   fir_response = estimate_FIR_response(v_raw, v_target, P)
   freq, freq_response = freqz(fir_response)
   return freq, freq_response


def reverse_equalizer(input_file_name_raw, input_file_mixed, outut_file_name, n_samples = 1028, n_samples_before = 50, P = 128, fs = 44100):
    """
    n_samples: Length of extract of file (starte at maximum amplitude), take power of 2
    n_samples_before: # Samples before maximum that are used in the extract
    P: Filter order
    """
    # FIXME: Assumes two channels in the signal. Calculates transfer funciton on one of the channels and
    # applies it to both.
    fs, v_raw = read(input_file_name_raw)
    v_raw_0 = v_raw[:,0]
    v_raw_1 = v_raw[:,1]

    fs, v_target = read(input_file_mixed)
    v_target_0 = v_target[:,0]
    v_target_1 = v_target[:,1]

    # Get a good extract
    n_start = max(np.argmax(v_raw_0) - n_samples_before, 0)
    sequence_0 = [n_start,n_start + n_samples] # about one beat

    # Get a good extract
    n_start = max(np.argmax(v_target_0) - n_samples_before, 0)
    sequence_1 = [n_start,n_start + n_samples] # about one beat

    freq, freq_response = estimate_frequency_response(v_raw_0[sequence_0[0]:sequence_0[1]], v_target_0[sequence_1[0]:sequence_1[1]], P)
    freq_response = np.abs(freq_response)
    freq = freq * n_samples * 2.0 * np.pi
    tf_fun = interp1d(freq, freq_response, fill_value= 'extrapolate')

    t_new, v_mod_0 = apply_equalizer(v_raw_0, tf_fun)
    t_new, v_mod_1 = apply_equalizer(v_raw_1, tf_fun)
    v_mod = np.hstack((np.reshape(v_mod_0, (len(v_mod_0), 1)), np.reshape(v_mod_1, (len(v_mod_1), 1))))

    write_wav(outut_file_name, fs, v_mod.astype('int16'))

    return tf_fun